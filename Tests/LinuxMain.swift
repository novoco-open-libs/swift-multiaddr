import XCTest

import SwiftMultiAddrTests

var tests = [XCTestCaseEntry]()
tests += SwiftMultiAddrTests.allTests()
XCTMain(tests)
